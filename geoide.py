from PyQt5.QtGui import QIcon
from PyQt5.QtCore import QVariant
from PyQt5.QtWidgets import QApplication, QAction, QDialogButtonBox

import io
from qgis.core import (
    QgsProject, QgsVectorLayer, QgsVectorDataProvider,
    QgsCoordinateReferenceSystem, QgsCoordinateTransform,
    QgsWkbTypes, QgsField, NULL
)

from qgis.core import QgsMessageLog

from .resources import *
from .geoide_dialog import GeoideDialog
from .geoide_result_dialog import GeoideResultDialog
import os
import sys
import math
import numpy as np
#from .pyalti.alti.grid import grid
root = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.join(root, "pyalti","alti"))
from grid import grid
import os
root = os.path.dirname(__file__)
rel_path = os.path.join("./Plugin1_BEA/pyalti/alti/grid/", "")

class Geoide:

    def __init__(self, iface):
        self.iface = iface
        self.project = QgsProject.instance()
        self.first_start = None
        self.dialog = None
        self.comboBoxLayers = []
        self.list_localisation = ["Métropole", "Corse", "Guadeloupe", "La Désirade", "Les Saintes", "Martinique",
                                  "Marie-Galante", "La Réunion",
                                  "Saint-Barthélémy", "Saint-Martin", "Les Îles Kerguelen", "Mayotte", "Bora Bora",
                                  "Huahine", "Maiao",
                                  "Maupiti", "Raiatea", "Tahaa", "Tupai", "Hiva Oa", "Nuku Hiva", "Fakarava", "Gambier",
                                  "Hao", "Mataiva",
                                  "Raivavae", "Reao", "Rurutu", "Tikehau", "Tubuai", "Moorea", "Tahiti",
                                  "Saint-Pierre et Miquelon"]
        self.grille_choisie = self.localisationSelectionChange(0)  # initialisation à la métropole (objet grille altimétrique)
        self.resultDialog = ""

    def initGui(self):
        icon_path = QIcon(":/plugins/geoide/icon.png")
        self.action = QAction(icon_path, "Récupération du géoide", self.iface.mainWindow())
        self.action.setEnabled(True)
        self.action.setCheckable(False)
        # associe le clic sur l'icône à la méthode run (ouverture du plugin)
        self.action.triggered.connect(self.run)
        self.iface.addToolBarIcon(self.action)

        self.first_start = True

    def unload(self):
        self.iface.unregisterMainWindowAction(self.action)
        self.iface.removeToolBarIcon(self.action)

    def run(self):
        # on crée l'interface, uniquement lors de la première ouverture
        if self.first_start:
            self.dialog = GeoideDialog(self.iface.mainWindow())
            # lorsqu'on sélectionne une couche dans la liste, on appelle layerSelectionChange
            # pour mettre à jour l'affichage du nombre de points dans la couche et activer
            # le bouton "OK"
            self.dialog.layersComboBox.currentIndexChanged.connect(self.layerSelectionChange)
            self.dialog.comboBoxLocalisation.currentIndexChanged.connect(self.localisationSelectionChange)
            self.first_start = False

        self.comboBoxLayers.clear()
        self.dialog.layersComboBox.clear()
        self.clearDialog()
        # on désactive le bouton "OK" car pour le moment on sait pas si on a une couche
        self.dialog.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False)

        # crée une liste des couches
        for layer in self.project.mapLayers().values():
            # on garde uniquement les couches de type points
            if isinstance(layer, QgsVectorLayer) and layer.wkbType() == QgsWkbTypes.Point:
                # et qui sont éditables
                if layer.dataProvider().capabilities() & QgsVectorDataProvider.AddAttributes:
                    self.comboBoxLayers.append(layer)

        # remplit la ComboBox de l'interface avec la liste des couches
        self.dialog.layersComboBox.addItems(layer.name() for layer in self.comboBoxLayers)
        # remplit la combobox qui propose les localisations du sol français
        self.dialog.comboBoxLocalisation.addItems(self.list_localisation)

        # si on est sur une couche points dans Qgis, on la sélectionne par défaut dans le plugin
        if self.iface.mapCanvas().currentLayer() is not None:
            self.dialog.layersComboBox.setCurrentText(self.iface.mapCanvas().currentLayer().name())

        # à l'appui sur "OK"
        if self.dialog.exec_():
            # on crée déjà la fenêtre de résultat
            resultDialog = GeoideResultDialog(self.iface.mainWindow())
            self.resultDialog = resultDialog
            # on initialise la progress bar à la valeur 0
            resultDialog.progressBar.setValue(0)
            # on lui indique le maximum qu'elle pourra atteindre
            resultDialog.progressBar.setMaximum(layer.featureCount())
            resultDialog.resultLabel.setText("Patience...")
            resultDialog.result2Label.clear()
            resultDialog.resultVoidLabel.hide()
            resultDialog.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False)
            resultDialog.open()
            QApplication.processEvents()

            # récupère la couche sélectionnée dans la liste
            layer = self.comboBoxLayers[self.dialog.layersComboBox.currentIndex()]
            layer.commitChanges()

            # fonction qui servira à convertir les coordonnées en lat/lon
            self.to4326 = QgsCoordinateTransform(layer.sourceCrs(), QgsCoordinateReferenceSystem.fromEpsgId(4326), self.project)

            nb_points = layer.featureCount()

            nb_updated_points = self.get_geoide_for_layer(layer)

            if nb_updated_points != nb_points:
                resultDialog.resultVoidLabel.show()
            resultDialog.buttonBox.button(QDialogButtonBox.Ok).setEnabled(True)

    def layerSelectionChange(self, idx):
        if idx == -1:
            return self.clearDialog()

        layer = self.comboBoxLayers[idx]

        # on met à jour le nombre de points affiché
        self.dialog.numberFeaturesLabel.setText("{} points".format(layer.featureCount()))

        # on a une couche sélectionnée, on active le bouton "OK"
        self.dialog.buttonBox.button(QDialogButtonBox.Ok).setEnabled(True)

    def localisationSelectionChange(self, idx):
        if idx == -1:
            return self.clearDialog()
        # faire le lien avec le dictionnaire des adresses noms des grilles
        # creer objet grille en fonction puis le mettre dans l'attribut self .localisation
        localisation = self.list_localisation[idx]

        #Je différencie à la main les anciennes des nouvelles grilles
        if localisation in ["Guadeloupe", "Les Saintes", "Guyane", "Saint-Barthélémy", "La Désirade", "Saint-Martin", "Métropole"]:
            self.grille_choisie = grid(localisation)
        else :
            self.grille_choisie = rel_path +localisation+".mnt"

    def clearDialog(self):
        self.dialog.numberFeaturesLabel.clear()

    def create_fields(self, layer, fields):
        # crée un attribut pour la couche layer
        # fields = (nom du champ, type)
        for field_name, field_type in fields:
            # on vérifie que l'attribut n'existe pas déjà
            if layer.fields().indexFromName(field_name) == -1:
                layer.dataProvider().addAttributes([QgsField(field_name, field_type)])
                layer.updateFields()

    def get_geoide_for_layer(self, layer):
        layer.startEditing()

        # on crée deux attributs pour la valeur du géoide
        self.create_fields(layer, (("geoide_m", QVariant.Double), ("geoide_ft", QVariant.Double)))

        nb_updated_points = 0

        for feature in layer.getFeatures():
            # on récupère la lat/lon du point
            point = self.to4326.transform(feature.geometry().asPoint())
            lat = point.y()
            lon = point.x()

            # fonction qui prend lon et lat en entrée
            # et qui retourne l'élévation
            geoide = self.get_geoide_for_point(lat, lon)

            if geoide is not None:
                feature["geoide_m"] = int(geoide)
                feature["geoide_ft"] = int(geoide) * 3.28084
                nb_updated_points += 1

                #if nb_updated_points % int(layer.featureCount()/100) == 0:
                    # on met à jour la progress bar en fonction du nombre du nombre de points dans le fichier de vol qu'elle a traités
                    #self.resultDialog.progressBar.setValue(nb_updated_points)

            layer.updateFeature(feature)

        layer.commitChanges()

        self.resultDialog.progressBar.setValue(layer.featureCount())
        return nb_updated_points

    # la fonction qui va récuperer l'élévation pour un point dans la couche du fichier de vol
    def get_geoide_for_point(self, lat, lon):

        #Dans le cas où il s'agit d'une grille au nouveau format IGN
        if type(self.grille_choisie) != type(""):
            try:
                return self.grille_choisie.interpolate(lon, lat)
            except Exception:
                return None

        #Dans le cas où il s'agit d'une grille à l'ancien format IGN
        else:
            pas_lon = 0
            pas_lat = 0
            with open(self.grille_choisie, "r") as file:
                for index, line in enumerate(file):
                    if line != "":
                        # Je créé une liste à partir des grilles de conversion altimétriques
                        if index == 0:
                            line = line.strip().split()
                            entete = line
                            lon_min = float(entete[0])
                            lon_max = float(entete[1])
                            lat_min = float(entete[2])
                            lat_max = float(entete[3])
                            pas_lat = float(entete[4])
                            pas_lon = float(entete[5])

                            if (lat < lat_min or lat > lat_max) or (lon < lon_min or lon > lon_max):
                                return None
                        else:
                            line = line.strip().split()
                            lon_grille = float(line[0])
                            lat_grille = float(line[1])
                            # si on trouve une latitude et une longitude assez proche de celle de l'avion dans la grille de conversion
                            if abs(lon_grille - lon) <= pas_lon and abs(lat_grille - lat) <= pas_lat:
                                geoide = float(line[2])
                                return geoide
