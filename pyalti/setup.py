"""
alti - Python altitudes management package
Copyright (C) 2016, Jacques Beilin <jacques.beilin@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""

from setuptools import setup
import os, os.path, sys

if sys.platform.startswith('win'):
    install_reqs = ['pywin32']
else:
    install_reqs = []

setup(name='alti',
        version="1.0.4",
        description='alti - Python altitudes management package',
        author='Jacques Beilin',
        author_email='jacques.beilin@gmail.com',
        install_requires = install_reqs,
        url='http://www.ensg.eu',
        packages=['alti'],
        package_data = {'alti': ['grid/*.mnt'],},
        py_modules=['grid','NormalField']
    )

