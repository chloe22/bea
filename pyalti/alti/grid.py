# -*- coding: utf-8 -*-
"""
Copyright (C) 2014-2019, Jacques Beilin <jacques.beilin@gmail.com>

License OL/LO
"""

import re
import math
import time
import sys
import copy
import numpy as np
import os
import logging

def lat2latiso(phi, f=1/298.257222101):
    """
     Ce programme permet de calculer la latitude isometrique d'un point ou d'un vecteur

     Usage : lat2latiso(phi)
     Entrees
     	phi : latitude en radian
         f : applatissement de lellipsoide, par defaut celui de IAG GRS80 

     Sorties
     	L   : latitude isometrique
    """

    e = (2*f-f**2)**0.5
    L = (np.log(np.tan(np.pi/4 + phi/2))) - e / 2 * np.log ((1+e*np.sin(phi))/(1-e*np.sin(phi)))
    return L

def rotation(u, theta, x):
    """
    Rotation du vecteur x par un rotation d'axe u, d'angle theta (radian)
    
    """
    
    u = u / np.linalg.norm(u)
    ux = u[0]
    uy = u[1]
    uz = u[2]

    # PASSAGE AXE -> MATRICE ROTATION
    # SOURCE : https://fr.wikipedia.org/wiki/Matrice_de_rotation
    P = np.array([[ux**2, ux*uy, ux*uz], [ux*uy, uy**2, uy*uz], [ux*uz, uy*uz, uz**2]])
    I = np.eye(3)
    Q = np.array([[0, -uz, uy], [uz, 0, -ux], [-uy, ux, 0]])    
    
    R = P + np.cos(theta) * (I - P) + np.sin(theta) * Q
    
    x = x.reshape((3,1))
    
    y = R.dot(x)
    
    return y.reshape(3,)
    

class node():
    """ noeud d'une grille
    A minima,  contient 1 valeur -> ondulation du geoide N
    peut aussi contenir un triplet TX, TY, TZ -> cas avec 3 valeurs
    peut aussi contenir un code de precision"""

    def __init__(self, ix, iy, lon, lat, value):
#        pass
        self.ix = ix
        self.iy = iy
        self.lon = lon
        self.lat = lat
        self.value = value

    def __str__(self):
        s = 'Node lon = %.5f dd,  lat = %.5f dd,  Value = %.4f,  ix = %d,  iy = %d' \
                % (self.lon, self.lat, self.value, self.ix, self.iy)
        return s

    def print(self):
        """Ecriture des attributs de la classe """
        print(self.__str__())
#        for s in self.__dict__:
#            print('%-35s : ' %(s),  self.__dict__.get(s))

class grid():
    """Classe de gestion des grilles de référence altimétrique de Circe

    Jacques Beilin - ENSG/DPTS - 2016-06-15

    """

    def __init__(self, name='none'):
        """ Initialisation de la grille

        Input :
         - name : nom d'une grille

        Si le paramètre "name" n'est pas fourni,  une instance est crée mais aucune grille
        n'est chargée.
        Si name correspond à un nom de grille fournie avec le package,
        alors la fonction init charge cette grille.

        Grilles fournies avec le package :
        - egm96_w.mnt : grille EGM96 monde entier
        - egm08fr.mnt : grille EGM08 restreinte à la France
        - raf98.mnt : ancienne surface de référence altimétrique pour la France continentale RAF98
        - ggf97cor.mnt : ancienne surface de référence altimétrique pour la Corse GGF97COR
        - rac09.mnt : surface de référence altimétrique pour la France continentale RAF09
        - raf09.mnt : surface de référence altimétrique pour la Corse RAC09
        - raf09_calern.mnt : surface de référence altimétrique pour la France continentale
          (RAF09 restreint à la zone de Calern)
        - gr3df97a.mnt : grille de conversion 3D NTF->RGF93
        """

        if name == 'none':
            pass
        else:
            path = os.path.dirname(os.path.abspath(__file__))
            #s = sys.modules['alti']
            #path = s.__path__[0]
            GridPath = '%s/grid/%s.mnt' % (path, name.lower().strip())
            self.loadCirceGrid(GridPath)

    def __str__(self):
        """Ecriture des attributs de la classe """
        sOut = ''
        for s in self.__dict__:
            if s == 'val':
                continue

            if s == 'Vlat' or s == 'Vlon':
                continue

            if s == 'data':
                continue
            sOut += '%-35s : ' % (s) + str(self.__dict__.get(s)) + "\n"

        return sOut


    def print(self):
        """Ecriture des attributs de la classe """
        print(self.__str__())

    def loadGrid(self, filename, verbose=False):
        """ Chargement des grilles mnt ou gra """
        ret = 0
        if re.search('mnt$', filename, re.IGNORECASE):
            ret = self.loadCirceGrid(filename, verbose)
        elif re.search('gra$', filename, re.IGNORECASE):
            ret = self.loadGravsoftGrid(filename, verbose)
        return ret

    def loadGravsoftGrid(self, filename, verbose=False):
        """ Chargement des grilles GAVSOFT """
        if verbose:
            print("Reading Gravsoft grid %s\n" % (filename))
        # loading strings
        try:
            with open(filename, 'rt') as f:
                lines = f.readlines()
        except:
            print('Unable to open %s' % (filename))
            return -2

        tab = lines[0].split()
        if len(tab) == 0:
            return -1
        self.lat_S = float(tab[0])
        self.lat_N = float(tab[1])

        tab = lines[1].split()
        if len(tab) == 0:
            return -1
        self.lon_W = float(tab[0])
        self.lon_E = float(tab[1])

        tab = lines[2].split()
        if len(tab) == 0:
            return -1
        self.dlat = float(tab[0])
        self.dlon = float(tab[1])

        for i in range(3):
            del lines[0]

        # Nombre de noeud par degré de longitude (arrondi à 1e-6)
        self.NPixelByDegLon = int(1e6/self.dlon)*1e-6
        # Nombre de noeud par degré de latitude (arrondi à 1e-6)
        self.NPixelByDegLat = int(1e6/self.dlat)*1e-6

        # Calcul des champs derives
        self.Nlon = round(((self.lon_E-self.lon_W)/self.dlon)+1)
        self.Nlat = round(((self.lat_N-self.lat_S)/self.dlat)+1)
        self.size = round(self.Nlon*self.Nlat)

        self.ValByNode = 1
        self.code_prec = 0

        dataline = ""
        for line in lines:
            dataline += " " + line
        data_str = dataline.split()
        data = np.zeros((len(data_str)))
        for i in range(len(data_str)):
            data[i] = float(data_str[i])

        self.Vlon = np.linspace(self.lon_W, self.lon_E, self.Nlon)
        self.Vlat = np.linspace(self.lat_N, self.lat_S, self.Nlat)

        self.data = data.reshape(self.Nlat, self.Nlon)
#        print(data.shape)

        return 0


    def loadCirceGrid(self, filename, verbose=False):
        """  [GRD] = loadCirceGrid(file_name) : chargement en memoire d'une grille au format Circe

         Input :
         - file_name : nom complet du fichier de grille (chemin+nom)
         - verbose : mode verbeux

         Output :
         - self : remplissage de la classe

                |- lon_W          : limite ouest de la grille
                |- lon_E          : limite est de la grille
                |- lat_S          : limite sud de la grille
                |- lat_N          : limite nord de la grille
                |- dlon           : pas en longitude (dd)
                |- dlat           : pas en latitude (dd)
                |- NPixelByDegLon : nombre de noeud par degre de longitude
                |- NPixelByDegLat : nombre de noeud par degre de latitude
                |- Nlon           : nombre d'elements en longitude
            GRD-|- Nlat           : nombre d'elements en latitude
                |- header_line    : ligne 1 de l'entete
                |- name           : nom de la grille
                |- Val            : tenseur Nlat*Nlon*(self.coord * 2
                                                       + self.nb_val + self.code_prec)
                |                   permet de gérer les grilles du type grille transfo NTF-RGF93
                |- data           : tenseur Nlat*Nlon des ondulations ou des vecteurs TX, TY, TZ
                |- precision      : matrice Nlat*Nlon des parametres de precision
                |- Vlon           : vecteur des longitudes
                |- Vlat           : vecteur des latitudes


         -5.5 8.5 42 51.5 0.0333333333333 0.025 2 0 1 1 0. France continentale -
         NGF-IGN69 dans RGF93
         53.3573 99 53.3569 99 53.3546 99 53.3481 99 53.3361 99 53.3192 99 53.299 ...
        """

        if verbose:
            print("Reading Circe grid %s\n" % (filename))
        # loading strings
        try:
            with open(filename, 'rt') as f:
                lines = f.readlines()
        except:
            print('Unable to open %s' % (filename))
            return -2

        header_line = lines[0]
        tab = header_line.split()
        if len(tab) == 0:
            return -1
#        print(tab)

        self.lon_W = float(tab[0])
        self.lon_E = float(tab[1])
        self.lat_S = float(tab[2])
        self.lat_N = float(tab[3])

        self.dlon = float(tab[4])
        self.dlat = float(tab[5])
        # Nombre de noeud par degré de longitude (arrondi à 1e-6)
        self.NPixelByDegLon = int(1e6/self.dlon)*1e-6
        # Nombre de noeud par degré de latitude (arrondi à 1e-6)
        self.NPixelByDegLat = int(1e6/self.dlat)*1e-6

        self.ordre_rangement = int(tab[6])
        self.coord = int(tab[7])
        self.nb_val = int(tab[8])
        self.code_prec = int(tab[9])
        self.translations = float(tab[10])

        name = ''
        for i in range(11, len(tab)):
            name += ' ' + tab[i]

        self.name = name.strip()

        self.ValByNode = int(self.coord * 2 + self.nb_val + self.code_prec)

        # Calcul des champs derives
        self.Nlon = round(((self.lon_E-self.lon_W)/self.dlon)+1)
        self.Nlat = round(((self.lat_N-self.lat_S)/self.dlat)+1)
        self.size = round(self.Nlon*self.Nlat)



        self.val = np.zeros((self.Nlat, self.Nlon, (self.coord * 2 + self.nb_val + self.code_prec)))

#        print(self.ordre_rangement,  self.size,  self.val.shape)

        dataline = ""
        for i in range(1, len(lines)):
            dataline += " " + lines[i]
        data_str = dataline.split()
        data = []
        for s in data_str:
            data.append(float(s))
        data = np.asarray(data)

        self.Vlon = np.linspace(self.lon_W, self.lon_E, self.Nlon)
        self.Vlat = np.linspace(self.lat_N, self.lat_S, self.Nlat)

#        self.print()
#        print(data.shape)

        # reorganisation des grilles en fonction des ordres de rangement
        if self.ordre_rangement == 1:
            #    -5.5 10 41 52 0.1 0.1 1 0 3 1 0. 0. 0. Grille NTF-RGF93
            #    -165.027 -67.1 315.813 99 -165.169 -66.948 316.007 99 -165.312 -66.796 316.2 99
            data2 = data.reshape((self.Nlon, self.Nlat, self.ValByNode)).swapaxes(0, 2)
            precision = copy.copy(data2[self.ValByNode-1, :, :])
            data2 = data2[0:self.ValByNode, :, :].squeeze()
            data2 = data2[:, ::-1, :]
            self.data = data2

        elif self.ordre_rangement == 2:
#            print(data.shape)
            data2 = data.reshape((self.Nlat, self.Nlon, \
                                  self.ValByNode)).swapaxes(0, 2).transpose(0, 2, 1)
            if self.code_prec == 1:
                precision = copy.copy(data2[self.ValByNode-1, :, :])
                self.precision = precision.squeeze()
                self.data = np.delete(data2, 1, 0).squeeze()
            else:
                self.data = data2.squeeze()

        elif self.ordre_rangement == 3:
            pass
            # pas géré car pas de données pour tester !

        elif self.ordre_rangement == 4:
            data3 = data.reshape((self.Nlat, self.Nlon, \
                                  self.ValByNode)).swapaxes(0, 2).transpose(0, 2, 1)
            data2 = np.zeros(data3.shape)
            for i in range(round(self.ValByNode)):
                data2[i, :, :] = np.flipud(data3[i, :, :])
            if self.code_prec == 1:
                precision = copy.copy(data2[self.ValByNode-1, :, :])
                self.precision = precision.squeeze()
                self.data = np.delete(data2, 1, 0).squeeze()
            else:
                self.data = data2.squeeze()
                
    def writeCirceGrid(self, filename, name=""):
        
        nDecimal = 4
        template = "%%.%df " % (nDecimal)
        
        with open(filename, 'wt') as f:
            s = "%.15f %.15f %.15f %.15f %.15f %.15f " % (self.lon_W, self.lon_E, self.lat_S, self.lat_N, self.dlon, self.dlat)
            f.write(s)
            s = "2 0 1 0 0 " + name
            f.write(s + "\n")
            
            for l in range(self.Nlat):
                for c in range(self.Nlon):
                    f.write(template % (self.data[l,c]))
                f.write("\n")
            
        
        

    def gridToPtsFile(self, filename):
        """ creation d'un fichier de points (sequentiel) à partir d'une grille """

        L = []
        Vlon = np.arange(self.lon_W, self.lon_E+1e-6, self.dlon)
        Vlat = np.arange(self.lat_S, self.lat_N+1e-6, self.dlat)
#        cc = 0
        for c in range(len(Vlon)):
#            cc += 1
#            if cc>2:
#                break
            for l in range(len(Vlat)):
                N = self.getNode(Vlon[c]*d2r, Vlat[l]*d2r)
                try:
#                    print(Vlon[c], Vlat[l], N.value)
                    L.append("%.6f %.6f %.3f" % (Vlon[c], Vlat[l], N.value))
                except:
                    pass
#                    print(Vlon[c], Vlat[l])

        with open(filename, 'wt') as f:
            for l in L:
                f.write(l+"\n")
        f.close()

        return 0



    def diffRefGrid(self, gridRef):
        """ Soustraction de la grille gridRef à self """
#        G = grid()

        for ip in range(len(self.Vlat)):
            for il in range(len(self.Vlon)):
                N = self.getNode(self.Vlon[il]*d2r, self.Vlat[ip]*d2r)
                Nref = gridRef.getNode(self.Vlon[il]*d2r, self.Vlat[ip]*d2r)
                try:
                    self.data[ip, il] = N.value - Nref.value
                except:
                    print(self.Vlon[il], self.Vlat[ip])


        return 0

    def extractGrid(self, lonW, latS, lonE, latN, dLon, dLat):
        """ Extraction d'une sous-grille """
#        G = grid()

#        print(self.data.shape)
        
        nLon = int(np.floor((lonE - lonW) / dLon))
        nLat = int(np.floor((latN - latS) / dLat))
        lonE = lonW + nLon * dLon
        latS = latN - nLat * dLat
        
        Vlon = np.linspace(lonW, lonE, nLon)
        Vlat = np.linspace(latN, latS, nLat)
        #FIXME : recamcler les limites E et S du fait de l'arrondi
        dataOut = np.zeros((nLat, nLon))

        
#        print(nLon, nLat, dataOut.shape, Vlon.shape, Vlat.shape)
        d2r = math.pi/180
        for l in range(len(Vlat)):
            for c in range(len(Vlon)):
                lat = Vlat[l]
                lon = Vlon[c]
                T = self.interpolate(lon*d2r, lat*d2r)
#                print(T)
                dataOut[l,c] = T
                
        G = grid()
        G.lat_S = latS
        G.lat_N = latN
        G.lon_E = lonE
        G.lon_W = lonW
        G.Nlat = nLat
        G.Nlon = nLon
        G.dlat = dLat
        G.dlon = dLon
        G.nb_val = G.Nlat * G.Nlon
        G.Vlat = Vlat
        G.Vlon = Vlon
        G.ValByNode = 1
        G.data = dataOut
        
                
#        print(Vlat, Vlon)
        
        
        
        return G



    def getNode(self, lon, lat):
        """ Récupération du noeud de grille le plus proche du point fourni

         Input :
         - lon, lat : longitude et latitude en radian

         Output :
         - objet  :  | ix : indice en longitude dans la grille
                     | iy : indice en latitude dans la grille
                     | lon : longitude (rad)
                     | lat : latitude (rad)
                     | value : vecteur des valeurs au noeud de la grille

        """

        d2r = math.pi/180
        lon_d = lon / d2r
        lat_d = lat / d2r

        if lon_d < self.lon_W:
            return
        if lon_d > self.lon_E:
            return

        if lat_d < self.lat_S:
            return
        if lat_d > self.lat_N:
            return

        """ recherche des indices du point le plus proche """
        ix = np.argmin(abs(self.Vlon - lon_d))
        iy = np.argmin(abs(self.Vlat - lat_d))

        lon = self.Vlon[ix]
        lat = self.Vlat[iy]

#        print(self.data.shape)

        if self.ValByNode-self.code_prec > 1:
            value = np.zeros((self.ValByNode-self.code_prec))
            for i in range(self.ValByNode-self.code_prec):
                value[i] = self.data[i, iy, ix]
        else:
            value = self.data[iy, ix]

#        print(lon, lat, value)

        N = node(ix, iy, lon, lat, value)
        return N

    def interpolate(self, lon, lat):
        """ T = RAF09.interpolate(6.8*d2r, 43.825*d2r)

        Interpolation bilinéaire à partir des 4 noeuds entourant le point.

        Input :
         - lon, lat : longitude et latitude en degrés

        Output :
         - T  : vecteur des valeurs interpolées au point
         si T ne contient qu'une valeur,  alors la fonction renvoie un scalaire

        """

        d2r = math.pi/180
        #Avant lond et latd étaient divisés par d2r
        lond = lon
        latd = lat
        logging.info('Vlon.shape: %s | lond: %s' % (self.Vlon.shape, lond))
        ixSW = int((lond - self.lon_W) * self.NPixelByDegLon)
        iySW = int((-latd + self.lat_N) * self.NPixelByDegLat) +1
        if ixSW >= self.Nlon:
            ixSW = self.Nlon - 1
        if iySW == 0:
            iySW = 1

        LonSW = self.Vlon[ixSW]
        LatSW = self.Vlat[iySW]

        VecteurLon = np.zeros(2)
        VecteurLat = np.zeros(2)
        VecteurLon[0] = LonSW
        VecteurLon[1] = LonSW + self.dlon
        VecteurLat[0] = LatSW
        VecteurLat[1] = LatSW + self.dlat

        N1 = self.getNode(VecteurLon[0]*d2r, VecteurLat[0]*d2r)
        N2 = self.getNode(VecteurLon[0]*d2r, VecteurLat[1]*d2r)
        N3 = self.getNode(VecteurLon[1]*d2r, VecteurLat[0]*d2r)
        N4 = self.getNode(VecteurLon[1]*d2r, VecteurLat[1]*d2r)

        x = (lond - VecteurLon[0]) / (VecteurLon[1] - VecteurLon[0])
        y = (latd - VecteurLat[0]) / (VecteurLat[1] - VecteurLat[0])

        T = (1-x)*(1-y)*N1.value + (1-x)*y*N2.value + x*(1-y)*N3.value + x*y*N4.value
        T = T.squeeze()

        return T
    
    def writeGeoTiff(self, fileOut, epsg=4326):
        """ 
        Export to geoTiff
        """
        
        try:
            from osgeo import gdal, osr
        except: 
            print("ERROR : Unable to import osgeo modules.")
            return
        
        driver = gdal.GetDriverByName('GTiff')
        proj = osr.SpatialReference()
        proj.ImportFromEPSG(epsg)
               
        dataset = driver.Create( fileOut, self.Nlon, self.Nlat, 1, gdal.GDT_Float32 )
        dataset.SetGeoTransform((self.lon_W, self.dlon, 0, self.lat_N, 0, -self.dlat))
        dataset.SetProjection(str(proj))
        band = dataset.GetRasterBand(1)
        band.WriteArray(self.data)
        band.SetNoDataValue(-9999)
        
    def calcEtaKsi(self, lon, lat, distance = 5000):
        """
        [eta,ksi] = GRD.calcEtaKsi(lon,lat) : calcul des angles de deviation de la verticale
            but : permettre la correction de l'az astro :
        Az_g − Az_a = -eta * tan(lat) + cotan(Dz) (eta * cos(Az) − ksi * sin(Az))
        
        Input :
        - lon, lat : longitude et latitude (rad)
        - distance : distance pour le calcul de la pente, par defaut 5km
        Output :
        - eta : deviation de la verticale vers l'EST (rad)
        - ksi : deviation de la verticale vers le NORD (rad)
        """
        
#        eta = np.nan
#        ksi = np.nan
        d2r = np.pi / 180
        R = 6378137

        lon_d = lon / d2r
        lat_d = lat / d2r
        # test sur l'emprise de la grille
        if lon_d<self.lon_W:return
        if lon_d>self.lon_E:return
        if lat_d<self.lat_S:return
        if lat_d>self.lat_N:return

        # détermination des points encadrants
        dalpha = distance / R
        dlat = dalpha
        dlon = dalpha / np.cos(lat);
#        print(dlat, dlon)
        
        vLat = np.array([lat + dlat, lat, lat - dlat ])
        vLon = np.array([lon - dlon, lon, lon + dlon ])
        
        X,Y = np.meshgrid(vLon, vLat)
        Z = np.zeros(X.shape)
        for i in range(3):
            for j in range(3):
                Z[i,j] = self.interpolate(X[i,j], Y[i,j])
#        print(X,"\n\n",Y,"\n\n",Z)
        
        
        dX = dlon * R
        dY = dlat * R
        # Formule de Horn
        dZdX = ((Z[0,2] + 2 * Z[1,2] + Z[2,2]) - (Z[0,0] + 2 * Z[1,0] + Z[2,0])) / 8 / dX
        dZdY = ((Z[0,0] + 2 * Z[0,1] + Z[0,2]) - (Z[2,0] + 2 * Z[2,1] + Z[2,2])) / 8 / dY

        slope = np.arctan((dZdX**2 + dZdY**2)**0.5)
#        print(dZdX, dZdY, np.arctan2(dZdX, dZdY) ,np.arctan(dZdX / dZdY))
        aspect = np.arctan(dZdX / dZdY) 
        if dZdX < 0:   
            aspect += np.pi
        elif dZdY < 0:
            aspect += 2 * np.pi
            
        
#        print(slope / d2r, aspect / d2r % 360)
        
        # Vecteur définissant l'interection du plan horizontal avec le geoide
        v = np.array([np.sin(aspect + np.pi/2), np.cos(aspect + np.pi/2), 0])
        u = np.array([np.sin(aspect), np.cos(aspect), 0])
        
        ur = rotation(v, -slope, u)
#        print(u, ur)
        N = np.cross(v, ur)
#        print("Normale", N)

        eta = np.arctan(N[0]/N[2])
        ksi = np.arctan(N[1]/N[2])
        return eta,ksi


if __name__ == "__main__":

    tic = time.time()

    d2r = math.pi/180


#    RAF09 = grid()
#    RAF09.loadGrid('./grid/raf09.mnt', True)
#
#    RAF98 = grid()
#    RAF98.loadGrid('./grid/raf98.mnt', True)

#    RAF18 = grid()
#    RAF18.loadGrid('./grid/raf18.mnt', True)
#    RAF18.writeGeoTiff("./grid/raf18.tif")
#    lonW = 5.95
#    latS = 45.8
#    lonE = 7.60
#    latN = 47.7
#    dLon = 0.033333333333
#    dlat = 0.0250
#    eRAF18 = RAF18.extractGrid(lonW, latS, lonE, latN, dLon, dlat)
#    eRAF18.writeGeoTiff("./grid/e_raf18.tif")
#    
#    eta, ksi = RAF18.calcEtaKsi(2.5*d2r, 46*d2r, 5000)
#    print("eta (deviation de la verticale vers l'EST (rad)) : %f\nksi (deviation de la verticale vers le NORD (rad)): %f" % (eta, ksi))
    
#    SUI = grid()
#    SUI.loadGrid('./grid/geoide_ch.mnt', True)
#    SUI.export2GeoTiff("./grid/SUI.tif")
#    eSUI = SUI.extractGrid(lonW, latS, lonE, latN, dLon, dlat)
#    eSUI.export2GeoTiff("./grid/eSUI.tif")

#    EGM08fr = grid()
#    EGM08fr.loadGrid('./grid/egm08fr.mnt', True)
#
#    qgf16 = grid()
#    qgf16.loadGrid('../../grilles/QGF16.GRA', True)
#    qgf16.writeGeoTiff("./grid/qgf16.tif")
#    eqgf16 = qgf16.extractGrid(lonW, latS, lonE, latN, dLon, dlat)
#    eqgf16.writeGeoTiff("./grid/eqgf16.tif")
#    
#    qgf16.writeCirceGrid("./grid/qgf16.mnt", "qgf16")
#    qgf16_2 = grid()
#    qgf16_2.loadGrid("./grid/qgf16.mnt", True)
#    qgf16_2.writeGeoTiff("./grid/qgf16_2.tif")
    

#    print(RAF18.getNode(6*d2r, 42*d2r))
#    print(qgf16.getNode(6*d2r, 42*d2r))
#
#    print(RAF18.getNode(6*d2r, 51*d2r))
#    print(qgf16.getNode(6*d2r, 51*d2r))
#
#    print(RAF18.getNode(-5*d2r, 51*d2r))
#    print(qgf16.getNode(-5*d2r, 51*d2r))
#    qgf16.grid2pts('qgf16.pts')

#    print(RAF18.data[100, 100])
#    print(qgf16.data[100, 100])
#    RAF18.diffRefGrid(qgf16)
#    RAF18.gridToPtsFile('raf18_qgf16.pts')

    toc = time.time()
    print('%.3f sec elapsed ' % (toc-tic))
    