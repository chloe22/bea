# -*- coding: utf-8 -*-
"""
Copyright (C) 2014-2019, Jacques Beilin <jacques.beilin@gmail.com>

License OL/LO
"""

import re
import time
import math
import numpy as np

class NormalField():
    """ Classe de définitions des ellipsoïdes du champ normal """
    def __init__(self, name):

        self.name = name
        self.Omega_dot = 7292115*1e-11
        self.GM = 3.986005e14
        self.G = 6.67384e-11

        # paramètres de l'ellipsoide GRS80
        if re.search('GRS80', name, re.IGNORECASE):
            self.name = "IAG GRS80"
            self.a = 6378137.0
            self.b = 6356752.3141

        # parametres de l ellipsoide Hayford 1909
        elif re.search('Hayford', name, re.IGNORECASE):
            self.name = "Hayford 1909 - International 1924"
            self.a = 6378388.0
            self.f = 1/297.0
            self.b = self.a*(1-self.f)

        self.e2 = (self.a**2 -self.b**2)/self.a**2
        self.e2p = (self.a**2 - self.b**2)/self.b**2
        self.CalcSomiglianaConstants()

    def CalcSomiglianaConstants(self):
        """ Computation of all constants usefull for normal gravity process """

        a = self.a
        b = self.b
        omega = self.Omega_dot
        GM = self.GM

        m = omega**2 * a * a * b / GM
        f = (a-b)/a
        ep2 = (a**2-b**2)/b/b
        ep = math.sqrt(ep2)
        q0 = 1/2*(1+3/ep2)*math.atan(ep) -3/2/ep
        q0p = 3*(1+1/ep2)*(1-math.atan(ep)/ep)-1

        self.gamma_e = GM/(a*b)*(1-m-(m*ep*q0p)/(6*q0))
        self.gamma_p = GM/a/a*(1+m*ep*q0p/3/q0)
        self.m = m
        self.f = f

    def Somigliana(self, phi):
        """ Normal gravity at (phi, h)

        Input
        - phi : latitude (radians)

        Output
        - gamma : normal gravity at ellipsoide level (m.s-2)

        """
        if "f" not in self.__dict__:
            self.CalcSomiglianaConstants()

        num = self.a*self.gamma_e*(np.cos(phi)**2)+self.b*self.gamma_p*(np.sin(phi)**2)
        denom = (self.a**2*(np.cos(phi)**2)+self.b**2*(np.sin(phi)**2))**0.5
        g0 = num/denom

        return g0

    def Gamma(self, phi, h=0.0):
        """ Normal gravity at (phi, h)

        Input
        - phi : latitude (radians)
        - h : ellipsoid height (m)

        Output
        - gamma : normal gravity (m.s-2)

        """
        if "f" not in self.__dict__:
            self.CalcSomiglianaConstants()

        # Pesanteur normale au dessus de l'ellipsoide
        return self.Somigliana(phi) * (1 - 2*(1+self.f+self.m-2*self.f*np.sin(phi)**2)*h/self.a \
                               + 3 * (h/self.a)**2)


    def NormalGradient(self, phi, density=0):
        """ Normal gravity gradient at ellipsoid level
        Bruns formula applied on normal ellipsoid

        Physical Geodesy,  Hofmann-Wellenhof and Moritz
        formula 2-147,  2-148 : free air gradient if density = 0
        formula 3-43 : Poincaré-Prey reduction if density ! = 0

        Input
        - phi : latitude (radians

        Output
        - dgamma/dh or dg/dH = gradient m.s^-2/m

        """

        if "f" not in self.__dict__:
            self.CalcSomiglianaConstants()

        w = np.sqrt(1 - self.e2 * np.sin(phi)**2)
        N = self.a / w
        rho = self.a * (1 - self.e2) / w**3
        J = 1/2 * (1/N + 1/rho)

        gamma = self.Gamma(phi)
        dgdh = -2 * gamma * J + 4 * math.pi * 6.67e-11 * density * 1000 - 2 * self.Omega_dot**2

        return dgdh

def TestGradients(phi):
    """ Test function for gradient in normal field

    Input
    - phi : latitude (radians)

    Output
    - funcions prints gamma,  free air gradient,  and Poincaré-Prey reduction
    """

    E = NormalField('GrS80')
    print(E.name)

    if isinstance(phi, (float, int)):
        phi = [phi]

    for p in phi:
        print("g         = %.4f mGal" % (1e5 * E.Gamma(p)))
        print("dg/dH     = %.4f mGal/m" % (1e5 * E.NormalGradient(p, 2.67)))
        print("dgamma/dh = %.4f mGal/m" % (1e5 * E.NormalGradient(p)))


if __name__ == "__main__":

    tic = time.time()

    Vlat = [math.pi/4, math.pi/3]

    TestGradients(Vlat)


    TestGradients(math.pi/4)
    TestGradients(1)

    toc = time.time()
    print('%.3f sec elapsed ' % (toc-tic))
