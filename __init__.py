def classFactory(iface):
    from .geoide import Geoide
    return Geoide(iface)