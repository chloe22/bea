pyalti - Python altitudes management package
Copyright (C) 2020-2021, Chloé Marcon <chloe.marcon@ensg.eu>

License OL/LO

==============
Dependencies
==============

This package requires Python 3.4
Minimal dependencies : re, math, time, sys, copy, numpy
For function grid.writeGeoTiff : gdal and osr from osgeo

==============
Installation
==============
Installation is accomplished from the command line.

user@desktop:~/pyalti$ python3 setup.py install

The above command needs to be performed as root.

==============
For Developers
==============

Please examine the documentation embedded in the module for full details on
how to use this module. This can be accomplished by doing the following at
a command prompt:

user@desktop:~$ python
Python 3.4.3 (default, Feb 27 2014, 19:58:35) 
[GCC 4.6.3] on linux2
Type "help", "copyright", "credits" or "license" for more information.
>>> import alti
>>> help(alti)

You will then receive several screenfuls of documentation and examples you
may use.
